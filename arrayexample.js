//declare all of our global variables
var scores = [];
var score;
var i = 0;

while (true) {
    // ask the user for a value
    score = parseFloat(prompt("Please enter a positive int: "));

    // if they entered a -1, stop getting data
    if (score == -1) {
        break;
    }

    // store a valid number in the array
    scores[i] = score;
    i++;
}

// - first we have to sum the scores
function sum(scores) {
    var total_score = 0;
    for (var j = 0; j < scores.length; j++) {
        total_score += scores[j];
    }
    return total_score;
}

// calculate the score average
function average(scores) {
    var average_score = sum(scores) / scores.length;
    return average_score;
}

//Hurray we just did all the math things! Now we just need to print the results
alert(scores.toString());
alert(sum(scores));
alert(average(scores));