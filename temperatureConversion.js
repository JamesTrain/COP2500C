/*
 * COP2500C-Spring
 * Lab Assignment 2
 * Exercise 1 - Temperature Conversion
 * James Gallagher
 */

//Prompt the user to choose what they're starting with
var whichTemp = prompt("Would you like to convert from Fahrenheit or Celsius?", "Fahrenheit");

switch (whichTemp) {
    
  //If the user is converting Celsius to Fahrenheit  
  case "Celsius":
    var celsius = prompt('Please enter the temperature in Celsius:');
    var converted = (celsius * 9 / 5 + 32).toPrecision(4); //round to 4 numbers
    
    //Give the user their answer
    var message = celsius+'\xB0C is ' + converted + '\xB0F.';
    alert(message);
    console.log(message);
    break;
    
  //If the user is converting Fahrenheit to Celsius
  case "Fahrenheit":  
    var fahrenheit = prompt('Please enter the temperature in Fahrenheit:');
    var converted = ((fahrenheit - 32) * 5 / 9).toPrecision(4); //round to 4 numbers
    
    //Give the user their answer
    var message = fahrenheit+'\xB0F is ' + converted + '\xB0C.';
    alert(message);
    console.log(message);
    break;
    
  //Let the user know if their input was incorrect.
  default:
    alert('Invalid input, please type Fahrenheit or Celsius into the prompt.');
    console.log('Invalid input, please type Fahrenheit or Celsius into the prompt.');
}