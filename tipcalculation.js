/*
 * COP2500C-Spring
 * Lab Assignment 3
 * Problem 1 - Tip Calculator
 * James Gallagher
 * Plan:
 * First I am going to need to prompt the user for the information we need to complete the calculation.
 * Then, once we know how much the bill was and how much the user would like to tip, we need to store this in a var to use later.
 * If the input is invalid we need to let the user know instead of performing the calculations.
 * Once we have the info we need we can calculate the tip and the total amount including the tip.
 * Then give the user their results.
 */

 //Prompt user for input
var bill = Number(prompt("How much was the bill?"));
var tipPercent = Number(prompt("How much would you like to tip?"));

//Check if the user input will work
if ((isNaN(bill)) && (isNaN(tipPercent))) {
    alert("Please enter a valid check amount and tip percentage.")
}

//Nicely let the user know there was an error with their input.
else {
    //Perform Calculations
    var tip = bill * tipPercent;
    var total = bill + tip;
    //Give the user the results
    alert("Tip is " + tip);
    alert("Total bill is " + total);    
}