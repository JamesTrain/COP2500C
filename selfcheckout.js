/*
 * COP2500C-Spring
 * Lab Assignment 6
 * Problem 1 - Putting it all Together: Loops, Objects, and Arrays
 * James Gallagher
 * Plan:
 * 1. We'll need to build the array of items, each entry in the array will be an item object with two properties, price and quantity.
 * 2. Then we can make the three functions we need, one to calculate the subtotal, one to calculate the tax, and one to add them together for the total.
 * 3. The first function will be the most complex, it needs to multiply the prices by their respective quantities before adding all the items together.
 * 4. Once all our functions are created, we'll run the function to fill the array, then show the user the output of each function one by one.
 */

var items = getUserInput();  //Create the array of items

//Show the user their input
for (var i = 0; i < 3; i++) {
    console.log("Price of item " + (i+1) + ": $" + (items[i].price).toFixed(2) + "\nQuantity of item " + (i+1) + ": " + items[i].quantity);
}

//Show the user the results
console.log("Subtotal: $" + (subtotal(items)).toFixed(2) + "\nTax: $" + (tax(items)).toFixed(2) + "\nTotal: $" + (total(items)).toFixed(2));

//Get user input to populate the array
function getUserInput() {
    var items = [];
    for (var i = 0; i < 3; i++) {
        var item = { price: Number(prompt("Enter the price of item " + (i+1))), quantity: Number(prompt("Enter the quantity of item " + (i+1))) };
        items[i] = item;
    }
    return items;
}

//Multiply the prices by their quantities and add all the items together
function subtotal(x) {
    var sum = 0;
    for (var i = 0; i < 3; i++) {
        sum = sum + x[i].price * x[i].quantity;
    }

    return sum;
}

//Multiply the subtotal by 0.05
function tax(x) {
    return subtotal(x) * 0.05;
}

//Add the subtotal to the tax
function total(x) {
    return subtotal(x) + tax(x);
}