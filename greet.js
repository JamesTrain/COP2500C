/*
 * COP2500C-Spring
 * Lab Assignment #5
 * Problem 1 - Simple Function
 * James Gallagher
 * Plan:
 * 1. Create a function called greet with a parameter to pass name in, that will greet the user.
 * 2. Allow the user to put in the own name with a prompt
 * 3. Call the function, passing in the name variable
 */

 //greet function
function greet (name){
    alert("Hello, " + name + "!");
}

var name = prompt("What is your name?"); //get users name

greet(name); //call the function