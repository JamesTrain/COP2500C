/*
 * COP2500C-Spring
 * Lab Assignment 3
 * Problem 2 - Guess The Number
 * James Gallagher
 * Plan:
 * We're going to need to generate a number for the user to guess at, and also keep the game running until the user guesses correctly.
 * To do this I'm going to use a while loop that will continue to run the game forever unless the user get's the corect answer
 * Each time the user guesses, we need to check if the number is higher or lower than the secret number using if statements.
 * If one of those cases are true then we give the user their hint and start back at the beginning of the while loop.
 * If the number is not higher or lower than the secret number it must be the exact same at which point we can congradulate the user on winning!
 */

//declare variables and generate secret number
var won = false;
var secret_number = Math.floor(Math.random() * 10);

//While loop to continue game until won == true
while (won === false) {
    //Prompt for user guess
    var guess = prompt('Guess a number between one and ten.');

    //Check if the number is too low or too high
    if (guess < secret_number) {
        alert('Too low!');
    } 
    
    else if (guess > secret_number) {
        alert('Too high!');
    } 
    
    //Must be the correct number, tell the user how awesome they are
    else {
        alert('Congratulations! You guessed the right number.');
        won = true;
    }
}