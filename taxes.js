/*
 * COP2500C-Spring
 * Lab Assignment #5
 * Problem 2 - Taxes
 * James Gallagher
 * Plan:
 * 1. Declare a variable that will store user input (bill amount), and also one that will store the user's state.
 * 2. Create an if else statement to only apply the tax if the state is FL, if not only display the subtotal.
 * 3. If it is FL, show the user the subtotal and then multiply the subtotal by the tax rate to get their total tax and show them.
 * 4. Add the tax to the subtotal to get the final amount.
 * 5. Wrap the entire code in a function called Lab5Problem2 and then call call it.
 */

 //Main function
function Lab5Problem2() {

    //Set up our variables
    var amount = Number(prompt("What is order amount?"));
    var state = prompt("What is the state?");

    //Run this code if the state is FL
    if (state == "FL") {
        alert("The subtotal is: $" + amount);
        alert("Your sales tax is: $" + (amount * 0.07).toFixed(2)); //calculate tax amount
        alert("Your total bill is: $" + ((amount * 0.07) + amount).toFixed(2)); //calculate total with tax
    }

    //Run this code if the state is not FL
    else {
        alert("Your total is: $" + amount.toFixed(2));
    }
}

Lab5Problem2(); //Call the function