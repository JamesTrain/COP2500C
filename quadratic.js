/*
 * COP2500C-Spring
 * Lab Assignment #4
 * Problem 1 - Mathematical Functions
 * James Gallagher
 * Plan:
 * 1. We need to setup functions for mutliply, add, and square that all take variables n and m, or just n in the case of the square.
 * 2. Once we have all these functions, we can use them to calculate the quadratic forumula instead of using the math operators such as *.
 * 3. Now that we have a function to complete the quadratic equation, we need to get the information from the user, so we'll prompt for a,b,c,x.
 * 4. Call the quadratic formula function which will return the answer of the formula, and alert the user.
 */

//Multiplication function
function multiply(n, m) {
    return n * m;
}

//Addition function
function add(n, m) {
    return Number(n) + Number(m);
}

//Square function
function square(n) {
    return multiply(n, n);
}

//Quadratic Equation
function calculate_quadratic(a, b, c, x) {
    return add(multiply(a, square(x)), add(multiply(b, x), c));
}

//Get the information from the user
var a = prompt("enter value of a");
var b = prompt("enter value of b");
var c = prompt("enter value of c");
var x = prompt("enter value of x");

//Give the user their answer using all the functions we created, and passing the user values into the function.
alert(calculate_quadratic(a, b, c, x));