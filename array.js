/*
 * COP2500C-Spring
 * Lab Assignment #4
 * Problem 2 - Array Usage
 * James Gallagher
 * Plan:
 * 1. We'll need to get the information from the user in a loop, since then can add as many data points as they like to the array. We'll use array.push to add the values to the array.
 *    Since -1 will end the call for user input, we can use a while loop that stops running once -1 is entered.
 * 2. Now that the user has created an array, we can print it out using the toString() method.
 * 3. To add the array together we can utilize the reduce method, which will go through the array calling the function to add the previous and next values together (using the add function from problem 1).
 * 4. Since we already have the sum, we can easily find the average of the array by dividing the sum by the length of the array.
 */


 //Add two numbers together
function add(a, b) {
    return a + b;
}

//Find the sum of the array
function sum(x) {
    return x.reduce(add);
}

//Find the average of the array
function average(x) {
    return sum(arr)/x.length;
}

//Declare the array
var arr = [];

//Allow the user to input data until -1 is entered
while (i != -1) {
   var i = prompt("Enter a positive integer, enter -1 when finished.");
   if (i != -1){
       arr.push(Number(i)); //If the number is not -1, add it to the string
   }
}

//Give them all that juicy information we just processed for them
alert(arr.toString());
alert(sum(arr));
alert(average(arr));