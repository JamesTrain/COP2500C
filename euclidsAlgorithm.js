/*
 * COP2500C-Spring
 * Lab Assignment 2
 * Exercise 2 - Euclid's Algorithm
 * James Gallagher
 */

//Function to find the greatest common devisor
function r(m, n) {
  if (n == 0) {
    return m;
  } else {
    return r(n, m % n);
  }
}

//Prompt the user for two positive integers
var a = prompt("Enter a positive integer for m:");
var b = prompt("Enter a positive integer for n:");

var answer = r(a, b);

//Give the user their answer
alert('The Greatest Common Devisor of ' + a + ' and ' + b + ' is: ' + answer);
console.log(answer);